package main;

import java.util.ArrayList;

import autre.Etudiant;
import autre.Formation;
import autre.Groupe;
import autre.Identite;

public class Main {

	public static void main(String[] args) {
		Formation f=new Formation("Informatique");
		f.ajouterMatiere("Math�matiques", 3);
		f.ajouterMatiere("Fran�ais", 1);
		String[] prenoms={"Donovan","geg "," erg"};
		Identite id=new Identite("Tanguier",prenoms);
		
		Etudiant e=new Etudiant(id,f);
		e.ajout_note("Fran�ais", (double)12);
		e.ajout_note("Fran�ais", (double)12);
		e.ajout_note("Fran�ais", (double)18);
		e.ajout_note("Fran�ais", (double)6);
		e.ajout_note("Math�matiques", (double)1);
		Etudiant e2=new Etudiant(id,f);
		e2.ajout_note("Fran�ais", (double)8);
		e2.ajout_note("Fran�ais", (double)10);
		e2.ajout_note("Fran�ais", (double)5);
		e2.ajout_note("Fran�ais", (double)3);
		e2.ajout_note("Math�matiques", (double)18);
		Groupe g=new Groupe(f);
		g.ajoutEtudiant(e);
		g.ajoutEtudiant(e2);
		
		System.out.println(e.toString());System.out.println(e2.toString());
		
		System.out.println("Moyenne 1: "+e.moyenne_pour_une_matiere("Fran�ais")+ " // Moyenne 2: "+e2.moyenne_pour_une_matiere("Fran�ais"));
		System.out.println("Moyenne groupe en Francais : "+g.calcul_moyenne_matiere("Fran�ais"));
		System.out.println("Moyenne G�n�rale 1 : "+e.moyenne_generale()+" // Moyenne G�n�rale 2 : "+e2.moyenne_generale());
		System.out.println("Moyenne groupe : "+g.calcul_moyenne_generale());
		
	}
}
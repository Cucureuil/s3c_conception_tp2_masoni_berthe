package autre;

import java.util.ArrayList;


/**
 * Mod�lise un groupe d'�tudiant appartenant � une formation
 */
public class Groupe {

	/**
	 * ATTRIBUTS
	 */
	private Formation form;
	private ArrayList<Etudiant> etudiants;
	
	/**
	 * Constructeur de l'objet Groupe selon une formation
	 * @param f Formation, la formation du groupe
	 */
	public Groupe(Formation f){
		this.etudiants=new ArrayList<Etudiant>();
		if(f!=null) this.form=f;
	}
	
	/**
	 * Ajoute un �tudiant au groupe
	 * @param e Etudiant, l'�tudiant � ajouter
	 */
	public void ajoutEtudiant(Etudiant e){
		if(e!=null && !this.etudiants.contains(e)){
			if(this.form==e.getFormation()){
				this.etudiants.add(e);
			}
		}		
	}
	
	/**
	 * Supprime un �tudiant au groupe
	 * @param e Etudiant, l'�tudiant � ajsupprimerouter
	 */
	public void supprimeEtudiant(Etudiant e){
		if(e!=null && this.etudiants.contains(e)) 
			this.etudiants.remove(e);
	}

	/**
	 * Calcul la moyenne du groupe pour une mati�re donn�e
	 * @param Matiere String, la mati�re dont on veut calculer la moyenne
	 * @return double, la moyenne du groupe
	 */
	public double calcul_moyenne_matiere(String mat){
		double moyenne=0;
		for(Etudiant e: this.etudiants){
			moyenne+=e.moyenne_pour_une_matiere(mat);
		}
		if(moyenne!=0) moyenne/=this.etudiants.size();
		return moyenne;
	}
	
	/**
	 * Calcul la moyenne g�n�rale du groupe
	 * @return double, la moyenne g�n�rale du groupe
	 */
	public double calcul_moyenne_generale(){
		double moyenne=0;
		for(Etudiant e:this.etudiants){
			moyenne+=e.moyenne_generale();
		}
		if(moyenne!=0)moyenne/=this.etudiants.size();
		return moyenne;
	}
	
	/**
	 * trie la liste selon le comparateur rentr�
	 * @param c
	 */
	public void tri(Comparateur c) {
		ArrayList<Etudiant> tmp = new ArrayList<Etudiant>() ;
		int index ;

		while(!etudiants.isEmpty()) {
			index = 0 ;
			for(int i=0; i<etudiants.size() ; i++) {
				if(c.etreAvant(etudiants.get(i), etudiants.get(index))) {
					index = i ;
				}
			}
			tmp.add(etudiants.get(index)) ;
			etudiants.remove(index) ;
		}
		
		etudiants = tmp ;
	}
	
	/**
	 * Retourne la formation du groupe
	 * @return Formation, la formation du groupe
	 */
	public Formation getForm() {
		return form;
	}

	/**
	 * Retourne la liste des �tudiants
	 * @return ArrayList<Etudiant> la liste des �tudiants
	 */
	public ArrayList<Etudiant> getEtudiants() {
		return etudiants;
	}

}

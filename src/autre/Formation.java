package autre;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Classe qui repr�sente une formation d'enseignement
 */
public class Formation {

	/**
	 * ATTRIBUTS
	 */
	private String id;
	private HashMap<String,Double> mati�res;

	/**
	 * Constructeur
	 * @param idform String, l'id de la formation
	 */
	public Formation(String idform){
		if(!idform.isEmpty()) 
			this.id=idform;
		this.mati�res=new HashMap<String,Double>();
	}

	/**
	 * Ajoute une mati�re � la liste de mati�re de la formation
	 * @param mat String, la mati�re � ajouter
	 * @param coef double, le coefficient de la mati�re ajout�e
	 */
	public void ajouterMatiere(String mat, double coef){
		if(coef>0 && !(mat.isEmpty())){
			this.mati�res.put(mat, coef);
		}
	}

	/**
	 * Supprime la mati�re le param�tre de la liste des mati�res de la formation
	 * @param mat String, la mati�re � supprimer de la liste des mati�res
	 */
	public void supprimerMatiere(String mat){
		if(!mat.isEmpty()){
			if(this.mati�res.containsKey(mat)){
				this.mati�res.remove(mat);
			}
		}
	}

	/**
	 * Retourne le coefficient d'une mati�re plac�e en param�tre
	 * @param mat String, le nom de la mati�re
	 * @return double, le coefficient recherch�
	 */
	public Double getCoef(String mat){
		Double c=(double) 0;
		if(!mat.isEmpty()){
			if(this.mati�res.containsKey(mat)){
				c=this.mati�res.get(mat);
			}
		} 
		return c;
	}

	/**
	 * Retourne la liste des mati�res de la formation
	 * @return HashMap<String, Double>
	 */
	public ArrayList<String> getMati�res(){
		ArrayList<String> l=new ArrayList<String>();
		for(String mat : this.mati�res.keySet()) l.add(mat);
		return l;
	}

	/**
	 * M�thode d'affichage des donn�es de Formation
	 */
	@Override
	public String toString() {
		String s=this.id+"\n Mati�res de la formation : \n";
		for(String mat:this.mati�res.keySet()){
			s+="   "+mat+" coeff: "+this.getCoef(mat)+" \n";
		}
		return s;
	}	
}

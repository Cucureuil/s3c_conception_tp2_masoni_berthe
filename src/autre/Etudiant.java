package autre;

import java.util.*;

/**
 * Classe qui mod�lise un �tudiant
 */
public class Etudiant {

	/**
	 * ATTRIBUTS
	 */
	private Identite identit�;
	private Formation formation;
	private HashMap<String,ArrayList<Double>> r�sultats;

	/**
	 * Constructeur
	 * @param i Identite
	 * @param f Formation
	 */
	public Etudiant(Identite i,Formation f){
		if(f!=null) this.formation=f;
		if(i!=null) this.identit�=i;
		this.r�sultats=new HashMap<String,ArrayList<Double>>();
		for(String mat : this.formation.getMati�res()){
			this.r�sultats.put(mat, new ArrayList<Double>());
		}
	}

	/**
	 * Ajoute une note � la mati�re choisie
	 * @param mat String, la mati�re
	 * @param note Double, la note
	 */
	public void ajout_note(String mat, Double note){
		if(!mat.isEmpty() && note>=0 && note<=20){
			if(this.r�sultats.containsKey(mat)){
				ArrayList<Double> liste_note_mat=this.r�sultats.get(mat);
				liste_note_mat.add(note);
				this.r�sultats.put(mat, liste_note_mat);
			}
		}
	}

	/**
	 * Calcul la moyenne d'une mati�re pour l'�tudiant
	 * @param mat String, la mati�re
	 * @return double, la moyenne de l'�tudiant dans la mati�re
	 */
	public double moyenne_pour_une_matiere(String mat){
		double m=0;
		if(!mat.isEmpty() && this.r�sultats.containsKey(mat)){
			ArrayList<Double> liste_note_mat=this.r�sultats.get(mat);
			for(Double note : liste_note_mat){
				m+=note;
			}
			m/=liste_note_mat.size();
		}
		return m;
	}

	/**
	 * Calcul la moyenne g�n�rale de l'�tudiant
	 * @return Double, la moyenne g�n�rale
	 */
	public double moyenne_generale(){
		double m=0;
		int coef_total=0;
		for(String mat : this.r�sultats.keySet()){
			if(!this.r�sultats.get(mat).isEmpty()){
				double coef_mat=this.formation.getCoef(mat);
				double note_mat=this.moyenne_pour_une_matiere(mat);
				m+=note_mat*coef_mat;
				coef_total+=coef_mat;
			}
		}
		if(coef_total!=0) m/=coef_total;
		else m=-1;
		return m;
	}

	/**
	 * Affiche les caract�ristiques d'un Etudiant
	 */
	@Override
	public String toString() {
		String s= "Etudiant \n [identit�=" + identit� + ", \n formation=" + formation +"] \nliste des mati�res de l'�l�ve \n";
		for(String mat : this.r�sultats.keySet()){
			s+=" "+mat+"\n";
			for(Double note : this.r�sultats.get(mat)){
				s+="  "+note+"\n";
			}
		}
		return s;
	}

	/**
	 * Retourne R�sultats d'�tudiant
	 * @return HashMap<String,ArrayList<Double>>
	 */
	public HashMap<String,ArrayList<Double>> getResultats() {
		return this.r�sultats;
	}
	
	/**
	 * Retoune la formation de l'�tudiant
	 */
	public Formation getFormation(){
		return this.formation;
	}
	
	public Identite getIdentite() {
		return identit� ;
	}
}

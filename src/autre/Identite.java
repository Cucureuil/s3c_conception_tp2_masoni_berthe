package autre;

public class Identite {

	private static int nbEtud = 1; //permet de g�n�rer le nip automatiquement
	private int nip ;
	private String nom ;
	private String[] prenom ;
	
	/**
	 * Constructeur de la classe identite : permet de cr�er le profil de l'etudiant
	 * @param n nom de l'�tudiant
	 * @param p prenom de l�tudiant
	 */
	public Identite(String n, String[] p) {
		if(n!=null && p.length<=3) {
			nom = n ;
			//permet de g�n�rer des tableaux de 3 cases pour tous les pr�noms des �tudiants, quelque soit leur nb de pr�noms r�el
			if(p.length==3) {
				prenom = new String[3] ;
				for(int i=0 ; i<p.length ; i++)
					prenom[i]=p[i] ;
			}
			nip = nbEtud ;
			nbEtud++ ;
		}
	}
	
	public String toString(){
		String s=nip+" : "+nom+" "+prenom[0]+", "+prenom[1]+", "+prenom[2];
		return s;
	}
	
	public String getNom() {
		return nom ;
	}
	
	public String getPrenom() {
		String s = "" ;
		for(String p: prenom) {
			s+= p + " " ;
		}
		return s ;
	}
}

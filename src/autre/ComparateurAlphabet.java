package autre;

public class ComparateurAlphabet implements Comparateur{

	@Override
	public boolean etreAvant(Etudiant e1, Etudiant e2) {
		int differents = (e1.getIdentite().getNom().compareTo(e2.getIdentite().getNom())) ;
		boolean avant ;
		//si les noms sont les m�mes on compare sur les pr�noms
		if(differents == 0)
			avant = (e1.getIdentite().getPrenom().compareTo(e2.getIdentite().getPrenom())<0) ;
		else
			avant = e1.getIdentite().getNom().compareTo(e2.getIdentite().getNom())<0 ;
		return avant;
	}

	
}

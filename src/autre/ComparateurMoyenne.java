package autre;

public class ComparateurMoyenne implements Comparateur{

	@Override
	public boolean etreAvant(Etudiant e1, Etudiant e2) {
		return e1.moyenne_generale()>e2.moyenne_generale() ;
	}

	
}
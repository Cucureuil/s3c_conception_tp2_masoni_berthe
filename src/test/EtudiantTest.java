package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import autre.Etudiant;
import autre.Formation;
import autre.Identite;

/**
 * Test la classe Etudiant du package autre
 */
public class EtudiantTest {

	/**
	 * ATTRIBUTS
	 */
	private Etudiant e;
	private Formation f;
	private Identite id;
	
	/**
	 * Initialisation des attributs par defaut
	 */
	@Before
	public void init(){
		f=new Formation("Informatique");
		f.ajouterMatiere("Math�matiques", 3);
		f.ajouterMatiere("Fran�ais", 1);
		String[] prenoms={"Donovan","prenom2 "," prenom3"};
		Identite id=new Identite("Tanguier",prenoms);
		
		
	}
	/**
	 * Test l'ajout d'une note dans une mati�re
	 */
	@Test
	public void test_ajout_note() {
		Etudiant e=new Etudiant(id,f);
		e.ajout_note("Math�matiques", (double) 5);
		assertEquals("La premi�re note doit �tre 5 en Math�matiques",new Double(5),e.getResultats().get("Math�matiques").get(0));
	}
	
	/**
	 * Calcul de la moyenne sur une mati�re
	 */
	@Test
	public void test_calcul_matiere(){
		Etudiant e=new Etudiant(id,f);
		e.ajout_note("Fran�ais", (double)12);
		e.ajout_note("Fran�ais", (double)12);
		e.ajout_note("Fran�ais", (double)18);
		e.ajout_note("Fran�ais", (double)6);
		double moyenne=(double)e.moyenne_pour_une_matiere("Fran�ais");
		assertEquals("La moyenen doit �tre de 12", new Double(12),new Double(moyenne));
	}
	
	/**
	 * Calcul de la moyenne sur une mati�re
	 */
	@Test
	public void test_calcul_generale(){
		Etudiant e=new Etudiant(id,f);
		e.ajout_note("Fran�ais", (double)12);
		e.ajout_note("Fran�ais", (double)12);
		e.ajout_note("Fran�ais", (double)18);
		e.ajout_note("Fran�ais", (double)6);
		e.ajout_note("Math�matiques", (double)1);
		double moyenne=(double)e.moyenne_generale();
		assertEquals("La moyenne g�n�rale doit �tre de 3.75", new Double(3.75),new Double(moyenne));
	}
}

package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import autre.Comparateur;
import autre.ComparateurAlphabet;
import autre.ComparateurMoyenne;
import autre.Etudiant;
import autre.Formation;
import autre.Groupe;
import autre.Identite;

/**
 * Test de la classe Groupe
 */
public class GroupeTest {

	/**
	 * ATTRIBUTS
	 */
	private Formation f1,f2;
	private Groupe g;
	private Etudiant e1,e2,e3;
	private Identite i1,i2;

	/**
	 * Initialise les attributs de la classe
	 */
	@Before
	public void init(){
		f1=new Formation("Informatique");
		f2=new Formation("Chimie");
		
		f1.ajouterMatiere("Math�matiques", 3);
		f1.ajouterMatiere("Fran�ais", 1);
		
		g=new Groupe(f1);
		
		String[] prenoms1={"Mathias","Joris"," "};
		String[] prenoms2={"Alphonse","Titouan"," "};
		String[] prenoms3={"Cl�mentine","Cecille",""};
		
		i1=new Identite("Gerard",prenoms1);
		e1=new Etudiant(i1, f1);
		
		i2=new Identite("Dupont",prenoms2);
		e2=new Etudiant(i2,f2);
		
		Identite i3=new Identite("Anger",prenoms3);
		e3=new Etudiant(i3,f1);
		
		e1.ajout_note("Fran�ais", (double)12);
		e1.ajout_note("Fran�ais", (double)12);
		e1.ajout_note("Fran�ais", (double)18);
		e1.ajout_note("Fran�ais", (double)6);
		e1.ajout_note("Math�matiques", (double)1);
		e3.ajout_note("Fran�ais", (double)8);
		e3.ajout_note("Fran�ais", (double)10);
		e3.ajout_note("Fran�ais", (double)5);
		e3.ajout_note("Fran�ais", (double)3);
		e3.ajout_note("Math�matiques", (double)18);
	}

	/**
	 * Test la m�thode ajoutEtudiant
	 */
	@Test
	public void test_ajout() {
		// Execution
		g.ajoutEtudiant(e1);
		// Test
		assertEquals("L'�tudiant de m�me formation a �t� ajout�",true,g.getEtudiants().contains(e1));
		
		// Execution
		g.ajoutEtudiant(e2);
		// Test
		assertEquals("L'�tudiant d'une formation diff�rente n'a pas �t� ajout�",false,g.getEtudiants().contains(e2));
	}

	/**
	 * Test la m�thode supprimeEtudiant
	 */
	@Test
	public void test_suppression() {
		// Execution
		g.ajoutEtudiant(e1);
		g.supprimeEtudiant(e1);
		// Test
		assertEquals("L'�tudiant a bien �t� supprim�",false,g.getEtudiants().contains(e1));
	}
	
	/**
	 * Test la m�thode calcul_moyenne_matiere
	 */
	@Test
	public void test_moyenne_matiere(){		
		g.ajoutEtudiant(e1);
		g.ajoutEtudiant(e3);
		assertEquals("La moyenne pour le Fran�ais doit �tre de ",new Double(9.25),new Double(g.calcul_moyenne_matiere("Fran�ais")));
	}
	
	/**
	 * Test la m�thode calcul_moyenne_generale
	 */
	@Test
	public void test_moyenne_generale(){
		g.ajoutEtudiant(e1);
		g.ajoutEtudiant(e3);
		assertEquals("La moyenne g�n�rale du groupe doit �tre de ",new Double(9.4375),new Double(g.calcul_moyenne_generale()));
	}
	
	/**
	 * Test la m�thode de tri alphab�tique
	 */
	@Test
	public void test_tri_alphabetique(){
		// initialisation
		g.ajoutEtudiant(e1);
		g.ajoutEtudiant(e3);
		
		Comparateur compAlpha=new ComparateurAlphabet();
		
		// execution
		g.tri(compAlpha);
		// test
		assertEquals(" L'�l�ve 3 doit �tre le premier nom pour le tri alphab�tique","Anger",g.getEtudiants().get(0).getIdentite().getNom());
	}
	
	/**
	 * Test la m�thode de tri merite
	 */
	@Test
	public void test_tri_merite(){
		// initialisation
		g.ajoutEtudiant(e1);
		g.ajoutEtudiant(e3);
		
		Comparateur compMoy=new ComparateurMoyenne();
		
		// execution
		g.tri(compMoy);
		//test
		assertEquals(" L'�l�ve 3 doit �tre le premier nom pour le tri par moyenne","Anger",g.getEtudiants().get(0).getIdentite().getNom());
	}
}

package test;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

import autre.Formation;

public class FormationTest {

	Formation f ;
	
	@Before
	public void init() {
		f = new Formation("Form1") ;
	}
	
	/**
	 * Test si l'ajout de matiere se passe correctement
	 */
	@Test
	public void test_ajout_matiere_OK() {
		f.ajouterMatiere("Maths", 3.5);
		
		ArrayList<String> lmat = f.getMati�res() ;
		assertTrue("La matiere devrait avoir ete ajoutee a la liste", lmat.contains("Maths")) ;
		
		Double d = f.getCoef("Maths") ;
		Double attendu = 3.5 ;
		assertEquals("Le coefficient devrait etre de 3", attendu, d) ;
	}
	
	/**
	 * Verifie que l'ajout d'une matiere nulle est bloqu�
	 */
	@Test
	public void test_ajout_matiere_null() {
		f.ajouterMatiere("", 3.5);
		
		ArrayList<String> lmat = f.getMati�res() ;
		assertFalse("La matiere ne devrait pas avoir ete ajoutee a la liste", lmat.contains("")) ;
	}
	
	/**
	 * Test si la suppression de matiere se passe correctement
	 */
	@Test
	public void test_suppr_matiere_OK() {
		f.ajouterMatiere("Maths", 3.5);
		f.ajouterMatiere("COA", 10);
		f.supprimerMatiere("Maths");
		
		ArrayList<String> lmat = f.getMati�res() ;
		assertFalse("La matiere devrait avoir ete supprimee de la liste", lmat.contains("Maths")) ;
		assertEquals("Il devrait rester un element dans la liste", 1, lmat.size()) ;
	}
	
	/**
	 * Test la suppression d'une matiere qui n'existe pas
	 */
	@Test
	public void test_suppr_matiere_inexistante() {
		f.ajouterMatiere("Maths", 3.5);
		f.ajouterMatiere("COA", 10);
		f.supprimerMatiere("Reseau");
		
		ArrayList<String> lmat = f.getMati�res() ;
		assertEquals("Il devrait rester 2 elements dans la liste", 2, lmat.size()) ;
	}
	
	/**
	 * Test si le coefficient d'une matiere est bien renseign�
	 */
	@Test
	public void test_recherche_coeff() {
		f.ajouterMatiere("Maths", 3.5);
		f.ajouterMatiere("COA", 10);
		
		Double d = f.getCoef("COA") ;
		Double attendu = 10.0 ;
		
		assertEquals("Le coefficient devrait etre de 10", attendu, d) ;
	}
	
	/**
	 * Test que la recherche du coefficient d'une matiere qui n'existe pas
	 * retourne un r�sultat correct
	 */
	@Test
	public void test_recherche_coeff_inexistant() {
		f.ajouterMatiere("Maths", 3.5);
		f.ajouterMatiere("COA", 10);
		
		Double d= f.getCoef("R�seau") ;
		Double attendu = 0.0 ;
		
		assertEquals("Le coefficient devrait etre de 0", attendu, d) ;
	}

}

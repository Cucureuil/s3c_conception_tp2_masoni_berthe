@startuml

participant "main" as main
participant "e : Etudiant" as e
participant "f : Formation" as f
participant "resultat : HashMap<String, double>" as res
participant "matiere : HashMap<String, double>" as mat


activate main
main -> e : calculerMoyenne()
activate e

loop resultats r
	e -> res : keySet()
	activate res
	e <- res : String mat
	deactivate res
	e -> res : get(mat)
	activate res
	e <- res : List notes
	deactivate res
	e -> f : getCoeff(mat)
	activate f
	f -> mat : get(mat)
	activate mat
	f <- mat: double coeff
	deactivate mat
	e <- f : double coeff
	deactivate f
end

main <- e : double moyenne
deactivate e

@enduml